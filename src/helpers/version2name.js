'use strict'

module.exports = (version) => {
  // IMPORTANT: must match version2name in scripts/create-search-index.py
  // in the playbook repo
  return version.replace(/\./g, '_')
}
