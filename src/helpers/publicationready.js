'use strict'

module.exports = (page) => page &&
  page.component &&
  page.component.latest &&
  page.component.latest.asciidocConfig.attributes['publication-ready']
