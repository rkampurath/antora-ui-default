'use strict'

function isLatest (page) {
  return page.componentVersion.version === page.component.latestVersion.version
}

module.exports = (page) => {
  // refer to src/partials/nav-explore.hbs
  if (!page.componentVersion) {
    return ''
  }
  return page.componentVersion.displayVersion + (isLatest(page) ? ' (latest)' : '')
}
